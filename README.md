# Money

Work-through-the-book exercises from *Test Driven Development by Example* by Kent Beck.

## TODO

* ~~$5 + 10 CHF = $10 if rate is 2:1~~
* ~~$5 + $5 = $10~~
* ~~Return `Money` from $5 + $5~~ abandoned as unimportant as it is an internal detail
* ~~Bank.Reduce(Money)~~
* ~~Reduce Money with conversion~~
* ~~Reduce(Bank, string)~~
* ~~Sum.Plus()~~
* ~~Expression.Times()~~
* ~~Use Fixture to deduplicate test code~~ Didn't need a proper fixture, just a separate test class with constructor

Old List

* ~~$5 * 2 = $10~~
* ~~Make "amount" private~~
* ~~Dollar side effects?~~
* Money rounding?
* ~~equals()~~
* ~~hashCode()~~
* ~~Equal null~~
* ~~Equal object~~
* ~~5 CHF * 2 = 10 CHF~~
* ~~Dollar/Franc duplication~~
* ~~Common Equals()~~
* ~~Common Times()~~
* ~~Compare Francs with Dollars~~
* ~~Currency?~~
* ~~Delete TestFrancMultiplication()?~~

using Domain;

using Xunit;

namespace UnitTests;

public class MoneyTests
{
    [Fact]
    public void TestDollarMultiplication()
    {
        Money five = Money.Dollar(5);

        Assert.Equal(Money.Dollar(10), five.Times(2));
        Assert.Equal(Money.Dollar(15), five.Times(3));
    }

    [Fact]
    public void TestEquality()
    {
        Assert.True(Money.Dollar(5).Equals(Money.Dollar(5)));
        Assert.False(Money.Dollar(5).Equals(Money.Dollar(6)));
        Assert.False(Money.Franc(5).Equals(Money.Dollar(5)));
        Assert.False(Money.Dollar(5).Equals(5));
        Assert.False(Money.Dollar(5).Equals(null));
    }

    [Fact]
    public void TestHashCode()
    {
        Assert.NotEqual(Money.Dollar(5).GetHashCode(), Money.Franc(5).GetHashCode());
        Assert.NotEqual(Money.Dollar(5).GetHashCode(), Money.Dollar(6).GetHashCode());
        Assert.Equal(Money.Dollar(5).GetHashCode(), Money.Dollar(5).GetHashCode());
    }

    [Fact]
    public void TestFrankMultiplication()
    {
        var five = Money.Franc(5);

        Assert.Equal(Money.Franc(10), five.Times(2));
        Assert.Equal(Money.Franc(15), five.Times(3));
    }

    [Fact]
    public void TestCurrency()
    {
        Assert.Equal("USD", Money.Dollar(1).Currency());
        Assert.Equal("CHF", Money.Franc(1).Currency());
    }

    [Fact]
    public void TestSimpleAddition()
    {
        var five = Money.Dollar(5);
        IExpression sum = five.Plus(five);
        var bank = new Bank();

        Money reduced = bank.Reduce(sum, "USD");

        Assert.Equal(Money.Dollar(10), reduced);
    }

    [Fact]
    public void TestPlusReturnsSum()
    {
        Money five = Money.Dollar(5);

        IExpression result = five.Plus(five);
        Sum sum = (Sum)result;

        Assert.Equal(five, sum.Augend);
        Assert.Equal(five, sum.Addend);
    }

    [Fact]
    public void TestReduceSum()
    {
        IExpression sum = new Sum(Money.Dollar(3), Money.Dollar(4));
        var bank = new Bank();

        var result = bank.Reduce(sum, "USD");

        Assert.Equal(Money.Dollar(7), result);
    }

    [Fact]
    public void TestReduceMoney()
    {
        var bank = new Bank();

        var result = bank.Reduce(Money.Dollar(1), "USD");

        Assert.Equal(Money.Dollar(1), result);
    }

    [Fact]
    public void TestReduceMoneyDifferentCurrency()
    {
        var bank = new Bank();
        bank.AddRate("CHF", "USD", 2);

        var result = bank.Reduce(Money.Franc(2), "USD");

        Assert.Equal(Money.Dollar(1), result);
    }

    [Fact]
    public void TestIdentityRate()
    {
        Assert.Equal(1, new Bank().Rate("USD", "USD"));
    }
}

public class MixedCurrencyCalculationTests
{
    public IExpression FiveBucks;
    public IExpression TenFrancs;
    public Bank Bank;

    public MixedCurrencyCalculationTests()
    {
        FiveBucks = Money.Dollar(5);
        TenFrancs = Money.Franc(10);
        Bank = new Bank();
        Bank.AddRate("CHF", "USD", 2);
    }

    [Fact]
    public void TestMixedAddition()
    {
        Money result = Bank.Reduce(FiveBucks.Plus(TenFrancs), "USD");

        Assert.Equal(Money.Dollar(10), result);
    }

    [Fact]
    public void TestSumPlusMoney()
    {
        IExpression sum = new Sum(FiveBucks, TenFrancs).Plus(FiveBucks);
        Money result = Bank.Reduce(sum, "USD");

        Assert.Equal(Money.Dollar(15), result);
    }

    [Fact]
    public void TestSumTimes()
    {
        IExpression sum = new Sum(FiveBucks, TenFrancs).Times(2);
        Money result = Bank.Reduce(sum, "USD");

        Assert.Equal(Money.Dollar(20), result);
    }
}

namespace Domain;

public class Bank
{
    private readonly Dictionary<Pair, int> _rates = new();

    public Money Reduce(IExpression source, string toCurrency)
    {
        return source.Reduce(this, toCurrency);
    }

    public void AddRate(string fromCurrency, string toCurrency, int rate)
    {
        _rates.Add(new Pair(fromCurrency, toCurrency), rate);
    }

    public int Rate(string fromCurrency, string toCurrency)
    {
        if (fromCurrency.Equals(toCurrency))
            return 1;

        var rate = _rates[new Pair(fromCurrency, toCurrency)];
        return rate;
    }
}

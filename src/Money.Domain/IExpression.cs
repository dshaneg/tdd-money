namespace Domain;

public interface IExpression
{
    Money Reduce(Bank bank, string toCurrency);

    public IExpression Plus(IExpression addend);
    public IExpression Times(int multiplier);
}

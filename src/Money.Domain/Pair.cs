namespace Domain;

internal class Pair
{
    private readonly string _from;
    private readonly string _to;

    public Pair(string from, string to)
    {
        _from = from;
        _to = to;
    }

    public override bool Equals(object? obj)
    {
        return obj is Pair pair
            && Equals(pair);
    }

    public bool Equals(Pair? other)
    {
        return other is not null
            && _to.Equals(other._to)
            && _from.Equals(other._from);
    }
    
    public override int GetHashCode()
    {
        return HashCode.Combine(_to, _from);
    }
}

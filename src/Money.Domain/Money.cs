namespace Domain;

public class Money: IExpression
{
    public int Amount { get; private set; }
    protected string _currency = string.Empty;


    public static Money Dollar(int amount)
    {
        return new Money(amount, "USD");
    }

    public static Money Franc(int amount)
    {
        return new Money(amount, "CHF");
    }

    public Money(int amount, string currency)
    {
        Amount = amount;
        _currency = currency;
    }

    public IExpression Times(int multiplier)
    {
        return new Money(Amount * multiplier, _currency);
    }

    public IExpression Plus(IExpression addend)
    {
        return new Sum(this, addend);
    }

    public Money Reduce(Bank bank, string toCurrency)
    {
        int rate = bank.Rate(Currency(), toCurrency);
        return new Money(Amount / rate, toCurrency);
    }

    public string Currency()
    {
        return _currency;
    }

    public override string ToString()
    {
        return $"{Amount} {_currency}";
    }

    public override bool Equals(object? obj)
    {
        return obj is Money money
            && Equals(money);
    }

    public bool Equals(Money? other)
    {
        return other is not null
            && Amount == other.Amount
            && Currency().Equals(other.Currency());
    }
    
    public override int GetHashCode()
    {
        return HashCode.Combine(Amount, _currency);
    }
}

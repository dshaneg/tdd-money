namespace Domain;

public class Sum : IExpression
{
    public IExpression Augend { get; private set; }
    public IExpression Addend { get; private set; }

    public Sum(IExpression augend, IExpression addend)
    {
        Augend = augend;
        Addend = addend;
    }

    public IExpression Plus(IExpression addend)
    {
        return new Sum(this, addend);
    }

    public IExpression Times(int multiplier)
    {
        return new Sum(Augend.Times(multiplier), Addend.Times(multiplier));
    }

    public Money Reduce(Bank bank, string toCurrency)
    {
        var amount = Augend.Reduce(bank, toCurrency).Amount
            + Addend.Reduce(bank, toCurrency).Amount;
        return new Money(amount, toCurrency);
    }
}
